#include <iostream>
#include <vector>
#include "sdl.h"
#include "game_object.h"
#include "player.h"

static const int SCREEN_W = 640;
static const int SCREEN_H = 480;
static const int FRAME_DELAY = 1000 / 60;

void driver() {
	SDL::ThrowErrorHandler err_handler {};
	SDL::Sdl(SDL_INIT_VIDEO, err_handler);
	SDL::Window w {
		"Test",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_W,
		SCREEN_H,
		0,
		err_handler};
	SDL::Renderer r {
		w,
		SDL::Renderer::Driver::ANY,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC,
		err_handler};

	Player p {10, 460};

	std::vector<GameObject*> objects {&p};
	for (;;) {
		auto t0 = SDL::GetTicks();
		SDL::Event e = SDL::PollEvent();
		if (e.type == SDL_QUIT)
			break;

		r.set_draw_color(0, 0, 0, 255);
		r.clear();

		for (auto* o : objects) {
			o->update(e);
			o->draw(r);
		}

		r.present();

		auto frame_time = SDL::GetTicks() - t0;
		if (frame_time < FRAME_DELAY)
			SDL::Delay(FRAME_DELAY - frame_time);
	}
}

int main() {
	try {
		driver();
	} catch (SDL::Error& e) {
		std::cerr << e.what();
	}
	return EXIT_SUCCESS;
}
