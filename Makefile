CC = g++
CFLAGS = -Wall -Wextra -Wunused -pedantic
INCLUDE = -Iinclude -I/usr/include/SDL2

game: main.cpp bin/vector_2d.o include/player.h
	$(CC) $(INCLUDE) $(CFLAGS) -g -o game bin/vector_2d.o main.cpp -lSDL2

bin/vector_2d.o: src/vector_2d.cpp include/vector_2d.h
	$(CC) $(INCLUDE) $(CFLAGS) -c src/vector_2d.cpp -o bin/vector_2d.o

run: game
	./game
