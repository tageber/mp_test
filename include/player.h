#ifndef PLAYER_H
#define PLAYER_H
//-------------------------------------------------------------------
#include "game_object.h"
#include "vector_2d.h"
//-------------------------------------------------------------------

class Player : public GameObject {
public:
	Player(float x, float y)
	:pos_ {x, y}
	{
	}

	void update(const SDL::Event& e) override {
		vel_ += acc_;

		if (pos_.y < 470) {
			if (vel_.y < 3)
				vel_ += {0, +1};
		}
		else {
			vel_.y = 0;
		}

		if (e.type == SDL_KEYDOWN) {
			if (e.key.keysym.sym == SDLK_LEFT)
				vel_.x = (int)(vel_.x - 1) % 3;
			else if (e.key.keysym.sym == SDLK_RIGHT)
				vel_ += {+1, 0};
			else if (e.key.keysym.sym == SDLK_SPACE)
				vel_ += {0, -10};
		}
		else if (e.type == SDL_KEYUP) {
			if (e.key.keysym.sym == SDLK_LEFT ||
				e.key.keysym.sym == SDLK_RIGHT)
			{
				vel_.x = 0;
			}
			else if (e.key.keysym.sym == SDLK_SPACE) {
				vel_.y = 0;
			}
		}
		pos_ += vel_;
	}

	void draw(SDL::Renderer& r) override {
		r.set_draw_color(0, 0, 0xff, 0xff);
		SDL::Rect player {(int)pos_.x, (int)pos_.y, 10, 10};
		r.fill_rect(&player);
	}
private:
	Vector2D pos_;
	Vector2D vel_;
	Vector2D acc_;
};

//-------------------------------------------------------------------
#endif
