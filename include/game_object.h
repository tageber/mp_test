#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H
//-------------------------------------------------------------------
#include <cmath>
#include "sdl.h"
//-------------------------------------------------------------------

struct Velocity {
	Velocity(int x_, int y_, int resistance_ = 1)
	:x {x_}, y {y_}, resistance {resistance_}
	{
	}

	void decrease() {
		if (abs(x) < abs(resistance)) {
			x = 0;
		}
		if (abs(y) < abs(resistance)) {
			y = 0;
		}

		if (x > 0) x -= resistance;
		else if (x < 0) x += resistance;

		if (y > 0) y -= resistance;
		else if (y < 0) y += resistance;
	}

	int x;
	int y;
	int resistance;
};

class GameObject {
public:
	virtual void draw(SDL::Renderer& r) =0;

	virtual void update(const SDL::Event& e) =0;

	virtual ~GameObject() {}
};

//-------------------------------------------------------------------
#endif

