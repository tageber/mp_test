#ifndef VECTOR_2D_H
#define VECTOR_2D_H
//-------------------------------------------------------------------
//-------------------------------------------------------------------

class Vector2D {
public:
	Vector2D();
	Vector2D(float, float);

	float length();
	void normalize();

	Vector2D& operator+=(const Vector2D&);
	Vector2D& operator-=(const Vector2D&);

	Vector2D& operator*=(float);
	Vector2D& operator/=(float);

	friend Vector2D operator+(const Vector2D&, const Vector2D&);
	friend Vector2D operator-(const Vector2D&, const Vector2D&);
	friend Vector2D operator*(const Vector2D&, float);
	friend Vector2D operator/(const Vector2D&, float);

	float x, y;
};

Vector2D operator+(const Vector2D&, const Vector2D&);
Vector2D operator-(const Vector2D&, const Vector2D&);
Vector2D operator*(const Vector2D&, float);
Vector2D operator/(const Vector2D&, float);

//-------------------------------------------------------------------
#endif
