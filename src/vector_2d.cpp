#include "vector_2d.h"
//-------------------------------------------------------------------
#include <cmath>
//-------------------------------------------------------------------

Vector2D::Vector2D()
:x {0}, y {0}
{
}

Vector2D::Vector2D(float x_, float y_)
:x {x_}, y {y_}
{
}

float Vector2D::length() {
	return sqrt(x * x + y * y);
}

void Vector2D::normalize() {
	auto len = length();
	if (len > 0)
		(*this) *= 1 / len;
}

Vector2D operator+(const Vector2D& a, const Vector2D& b) {
	return Vector2D {a.x + b.x, a.y + b.y};
}

Vector2D& Vector2D::operator+=(const Vector2D& other) {
	x += other.x;
	y += other.y;
	return *this;
}

Vector2D operator-(const Vector2D& a, const Vector2D& b) {
	return Vector2D {a.x - b.x, a.y - b.y};
}

Vector2D& Vector2D::operator-=(const Vector2D& other) {
	x -= other.x;
	y -= other.y;
	return *this;
}

Vector2D operator*(const Vector2D& a, float f) {
	return Vector2D {a.x * f, a.y * f};
}

Vector2D& Vector2D::operator*=(float f) {
	x *= f;
	y *= f;
	return *this;
}

Vector2D operator/(const Vector2D& a, float f) {
	return Vector2D {a.x / f, a.y / f};
}

Vector2D& Vector2D::operator/=(float f) {
	x /= f;
	y /= f;
	return *this;
}

//-------------------------------------------------------------------
